terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.98.0"
    }
  }

  required_version = ">= 1.1.0"
}
provider "azurerm" {
  features {}
  subscription_id = "36a3208f-9672-41cc-b490-a793fc92210d"
  tenant_id       = "190ce420-b157-44ae-bc2f-69563baa5a3b"
}
resource "azurerm_resource_group" "Packer" {
  name     = "rscterra"
  location = "eastus"
}
