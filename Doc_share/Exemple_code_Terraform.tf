terraform {

   required_version = ">=0.12"

   required_providers {
     azurerm = {
       source = "hashicorp/azurerm"
       version = "~>2.0"
     }
   }
 }
#On définit ici le provider Azure
 provider "azurerm" {
   features {}
   subscription_id   = "personnal key"
   tenant_id         = "personnal key"
  
 }
#Exemple pour la creation d'un ressource group sur Azure
 resource "azurerm_resource_group" "test" {
   name     = "acctestrg"
   location = "West US 2"
 }