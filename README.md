# Documentation Terraform avec Azure #

## BUILD ##
Exécuter ces commandes en powershell "en tant qu'administrateur".

### Vérifier l'état de ExecutionPolicy
```
Get-ExecutionPolicy
```
### Set ExecutionPolicy
```
Set-ExecutionPolicy AllSigned
```

### Installation de chocolate
```
Set-ExecutionPolicy Bypass -Scope Process -Force;[System.Net.ServicePointManager]::SecurityProtocol =[System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

### Vérification de la bonnes installation de celui-ci
```
choco
```

### Installation de terraform via choco:
```
choco install terraform -Y
```

### Tester la commande terraform avec tout simplement
```
terraform
```

### Installation de la commande Azure "az" en CLI powershell : https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-powershell
```
$ProgressPreference = 'SilentlyContinue'; Invoke-WebRequest -Uri https://aka.ms/installazurecliwindows -OutFile .\AzureCLI.msi; Start-Process msiexec.exe -Wait -ArgumentList '/I AzureCLI.msi /quiet'; rm .\AzureCLI.msi
```

### Lien directe installation en MSI du package Azure CLI:
`https://aka.ms/installazurecliwindows`

Puis relancer votre instance powershell

### Tester la commande azure CLI
```
az
```
### Connecter votre powershell et suivez les instructions
```
az login
```

## RUN ##
### Workflow
![workflow](./Doc_share/workflow_terra.png)

### Documentation hashicorp pour azurerm (code terraform)
```
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs
```
### Initialisation
Editer un fichier main.tf pour y mettre l'ensemble des actions souhaitées que terraform doit executer.
Vous pouvez l'écrire avec notepad, bloc-notes, visual StudioCode...
Une fois ce fichier créé, plusieurs commandes vous seronts utile.

### Initialiser le projet terraform avec la commande
```
terraform init
```
### Voici le resultat de ce init:
```
PS C:\Users\<USER>\Documents\Terraform> terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/azurerm versions matching "~> 2.98.0"...
- Installing hashicorp/azurerm v2.98.0...
- Installed hashicorp/azurerm v2.98.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### Il faut à la suite de cela, effectuer un test du fichier "main.tf".
C'est l'équivalent d'un dry-run pour les test et nous retourne ce qu'il faut corriger la configuration.
```
terraform plan
```
### Voici ce que vous retourne terraform plan:
```
Plan: 11 to add, 0 to change, 0 to destroy.
```

### Après avoir tester votre main.tf il faudra executer la commande suivante pour executer votre main.tf et créer vos machines:
```
terraform apply
```
Celui-ci devrait vous retourné ceci, après validation écrite en saisisant "yes" :
```
Apply complete! Resources: 11 added, 0 changed, 0 destroyed.
```


### Une fois que vous avez finis de jouer avec vos machines créées sur Azure via Terraform vous pouvez également les supprimer.
La commande pour les supprimer est :
```
terraform destroy
```
Une fois machines détruit vous pouvez constater ce resultat suite à votre commande il vous demandera également une validation écrite pour la suppression avec "yes":
```
Destroy complete! Resources: 11 destroyed.
```
Vous pouvez, vérifier sur votre interface Azure que celle-ci ont bien été supprimé.

### Afficher en sortie les IP des machines
Pour permettre de générer plus tard notre inventory avec les IP des machines pour qu'Ansible puisse automatiquement se connecter sur les IP et mettre en place nos services, il nous fait générer notre sortie d'IP depuis Terraform, pour cela il faut mettre en place les outputs des IP :
```
output "PUBLIC ADRESSE LINUX" {
  description = DESCRIPTION
  value       = VARIABLE IP ADRESSE
}
```

Pour vous aider, cherchez sur Internet "Output terraform"

Ce qui va générer en sortie :
```
public_adresses_linux = "40.121.161.184"
```
Pour rendre l'Output exploitable il nous faut seulement l'IP sans le nom de la variable, Faites en sorte de ne sortir que les IP grâce local-exec, ce qui donnera en résultat final en Output :
```
40.121.161.184
```

### Exercice:
Vous devrez, déployer une machine CentOS 8 sur Azure à l'aide de Terraform.
Puis ajouter les services mysql, php, apache sur cette machine avec ansible.
